#==================================================================================================
# Author:	    David Zhang 
# Create date:  11/01/2019
# Description:  Convert HL7 message into JSON
#               This script is used by Logstash Ruby filter plugin
# Usage:        Hl7ToJsonConverter.py "hl7 message content"
# Version:      1.0
#==================================================================================================
import sys, json
from hl7apy.parser import parse_message

def hl7_str_to_dict(s, use_long_name=True):
    m = parse_message(s)
    return hl7_message_to_dict(m, use_long_name=use_long_name)

def hl7_message_to_dict(m, use_long_name=True):
    if m.children:
        d = {}

        for c in m.children:
            name = str(c.name).lower()

            if use_long_name:
                name = str(c.long_name).lower() if c.long_name else name

            dictified = hl7_message_to_dict(c, use_long_name=use_long_name)

            if name in d:
                if not isinstance(d[name], list):
                    d[name] = [d[name]]
                d[name].append(dictified)
            else:
                d[name] = dictified
        return d
    else:
        return m.to_er7() 

def main():	
    try:
        fileContent = sys.argv[1]
        if len(fileContent) > 0 and fileContent.startswith('MSH|'):
            fileContent = fileContent.replace('\\r', '\r')
            dict = hl7_str_to_dict(fileContent)
            js = json.dumps(dict, sort_keys=True, indent=None) 
            print(js)
    except:
		print("Unexpected error:", sys.exc_info()[0])
		print(fileContent)
		raise

if __name__ == '__main__':
    main()