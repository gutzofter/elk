﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;
using log4net;
using log4net.Appender;

namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    /// <summary>
    ///  A wrapper of an Logger object
    /// </summary>
    public class AppLogger
    {
        /// <summary>
        /// 
        /// </summary>
        public ILog Logger { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LogFileName { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public AppLogger()
        {

            Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(),
                       typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            //var appenders = Logger.Logger.Repository.GetAppenders();
            var appenders = repo.GetAppenders();
            if (appenders != null && appenders.Length > 0)
            {
                var appender = appenders[0];
                if (appender is FileAppender)
                {
                    LogFileName = ((FileAppender)appender).File;
                }
            }
            Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public void LogError(Exception ex)
        {
            if (Logger != null)
            {
                Logger.Error(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message)
        {
            if (Logger != null)
            {
                Logger.Error(message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void LogWarning(string message)
        {
            if (Logger != null)
            {
                Logger.Warn(message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void LogInfo(string message)
        {
            if (Logger != null)
            {
                Logger.Info(message);
            }
        }
    }
}

