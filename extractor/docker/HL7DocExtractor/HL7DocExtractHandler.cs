using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// 
/// </summary>
namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    /// <summary>
    /// 
    /// </summary>
    enum ATTACHMENTTYPE
    {
        NONE,
        DOC,
        MDOC,
        RAD
    }

    /// <summary>
    /// 
    /// </summary>
    enum MESSAGETYPE
    {
        NONE,
        ADT,
        RDE,
        ORU,
        VXU
    }

    /// <summary>
    /// 
    /// </summary>
    enum MSHFIELDS
    {
        ENCODING_CHARS = 1,
        SENDING_APPLICATIONS,
        SENDING_FACILITY,
        RECEIVING_APPLICATION,
        RECEIVING_FACILITY,
        DATETIME_OF_MESSAGE,
        SECURITY,
        MESSAGE_TYPE,
        MESSAGE_CONTROL_ID,
        PROCESSING_ID,
        VERSION_ID,
        SEQUENCE_NUMBER,
        CONTINUATION_POINTER,
        ACCEPT_ACKNOWLEDGEMENT_TYPE,
        APPLICATION_ACKNOWLEDGEMENT_TYPE,
        COUNTRY_CODE,
        CHARACTER_SET,
        PRINCIPAL_LANGUAGE_OF_MESSAGE,
        ALTERNATE_CHARATER_SET_HANDLING_SCHEME,
        MESSAGE_PROFILE_IDENTIFIER
    }

    /// <summary>
    /// 
    /// </summary>
    class MessageInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public  Message Hl7Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MESSAGETYPE Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReceivingDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ControlNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ATTACHMENTTYPE AttachmentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ISettings Settings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MemberNumber { get; set; }
        public string TransactionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MessageInfo()
        {
            Type = MESSAGETYPE.NONE;
            ReceivingDate = string.Empty;
            ControlNumber = string.Empty;
            AttachmentType = ATTACHMENTTYPE.NONE;
            MemberNumber = string.Empty;
            Settings = HL7DocExtractor.Settings;
            TransactionType = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        public MessageInfo(Message msg) : this()
        {
            Hl7Message = msg;
            SetProperties();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mshSeg"></param>
        private void SetProperties()
        {
            try
            {
                Type = ConvertMessageType(Hl7Message.MessageType);
                MemberNumber = Hl7Message.MemberNumber;
                ControlNumber = Hl7Message.MessageControlId;
                ReceivingDate = Hl7Message.DateTimeOfMessage;
                TransactionType = Hl7Message.TransactionType;
            }
            catch { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private MESSAGETYPE ConvertMessageType(string type)
        {
            if (MESSAGETYPE.ADT.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return MESSAGETYPE.ADT;
            }
            else if (MESSAGETYPE.RDE.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return MESSAGETYPE.RDE;
            }
            else if (MESSAGETYPE.ORU.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return MESSAGETYPE.ORU;
            }
            else if (MESSAGETYPE.VXU.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return MESSAGETYPE.VXU;
            }
            else
            {
                return MESSAGETYPE.NONE;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    class PdfAttachmentSeparator
    {
        /// <summary>
        /// 
        /// </summary>
        public string ObxSeg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PdfContent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ObxSegWoPdf { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PdfEncodedStrMarker
        {
            get
            {
                return @"|^^PDF^base64^";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public FileInfo AttachmentFileName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ISettings Settings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obxSeg"></param>
        public PdfAttachmentSeparator(string obxSeg, FileInfo attmFileName)
        {
            ObxSeg = obxSeg;
            AttachmentFileName = attmFileName;
            Settings = HL7DocExtractor.Settings;
            SeparatePdf();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SeparatePdf()
        {
            PdfContent = string.Empty;
            ObxSegWoPdf = ObxSeg;

            if (ObxSeg != null && ObxSeg.Length > PdfEncodedStrMarker.Length)
            {
                var pdfFieldStartIdx = ObxSeg.IndexOf(PdfEncodedStrMarker);
                if (pdfFieldStartIdx > 0)
                {
                    try
                    {
                        var pdfSubstr = ObxSeg.Substring(pdfFieldStartIdx + PdfEncodedStrMarker.Length);

                        var pdfFieldEndIdx = pdfSubstr.IndexOf(EnvironmentalVariable.Hl7FieldSeparator);
                        PdfContent = pdfSubstr.Substring(0, pdfFieldEndIdx);
                        ObxSegWoPdf = ObxSeg.Substring(0, pdfFieldStartIdx + 1) + PdfEncodedStrMarker + AttachmentFileName.Name + pdfSubstr.Substring(pdfFieldEndIdx);
                    }
                    catch { }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    class HL7AttachmentExtractHandler
    {
        /// <summary>
        /// 
        /// </summary>
        public  string FileNameConnector
        {
            get
            {
                return EnvironmentalVariable.Hl7FileNameSeparator;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] Segments { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PdfAttmExt
        {
            get
            {
                return ".pdf";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TxtAttmExt
        {
            get
            {
                return ".txt";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Hl7Ext
        {
            get
            {
                return ".hl7";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string MshSegName
        {
            get
            {
                return "MSH";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ObxSegName
        {
            get
            {
                return "OBX";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ObrSegName
        {
            get
            {
                return "OBR";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NteSegName
        {
            get
            {
                return "NTE";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string RawMsgstr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ExtMSgStr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MessageInfo MessageInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AppLogger Logger { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ISettings Settings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NewLinePattern
        {
            get
            {
                return @"\t|\n|\r";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int GetNotesFieldIndex(string seg)
        {
            try
            {
                var segName = seg.Substring(0, 3);
                if (ObxSegName.Equals(segName, StringComparison.OrdinalIgnoreCase))
                {
                    return 5;
                }
                else if (NteSegName.Equals(segName, StringComparison.OrdinalIgnoreCase))
                {
                    return 3;
                }
            }
            catch { }

            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hl7raw"></param>
        /// <param name="hl7FileInfo"></param>
        public HL7AttachmentExtractHandler(Message hl7msg)
        {
            Logger = HL7DocExtractor.Logger;
            Settings = HL7DocExtractor.Settings;
            MessageInfo = new MessageInfo(hl7msg);

            var line = string.Empty;
            var segs = new List<string>();
            var stream = new StreamReader(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(hl7msg.Content)));
            while (!stream.EndOfStream)
            {
                segs.Add(stream.ReadLine());
            }
            Segments = segs.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        public void ExtractAttachment()
        {
            var hl7Msg = new StringBuilder();
            var attm = new List<string>();

            if (Segments == null || Segments.Length == 0)
            {
                return;
            }

            foreach (var line in Segments)
            {
                var seg = ValidateSegment(line);
                if (string.IsNullOrEmpty(seg))
                {
                    continue;
                }

                if (seg.StartsWith(ObrSegName + EnvironmentalVariable.Hl7FieldSeparator) && MessageInfo.AttachmentType == ATTACHMENTTYPE.NONE)
                {
                    MessageInfo.AttachmentType = GetAttachmentType(seg);
                }
                if (seg.StartsWith(ObxSegName + EnvironmentalVariable.Hl7FieldSeparator) && MessageInfo.AttachmentType == ATTACHMENTTYPE.MDOC)
                {
                    var pdfAttachmentSeparator = new PdfAttachmentSeparator(seg, GetAttachmentFileName());
                    attm.Add(pdfAttachmentSeparator.PdfContent);
                    hl7Msg.Append(RemoveNonAsciiChar(pdfAttachmentSeparator.ObxSegWoPdf) + EnvironmentalVariable.Hl7CarriageReturn);
                }
                else if ((seg.StartsWith(NteSegName + EnvironmentalVariable.Hl7FieldSeparator) || seg.StartsWith(ObxSegName + EnvironmentalVariable.Hl7FieldSeparator)) &&
                            (MessageInfo.AttachmentType == ATTACHMENTTYPE.DOC || MessageInfo.AttachmentType == ATTACHMENTTYPE.RAD ))
                {
                    attm.Add(seg);
                }
                else
                {
                    hl7Msg.Append(RemoveNonAsciiChar(seg) + EnvironmentalVariable.Hl7CarriageReturn);
                }
            }

            // create the attachment file
            CreateAttachmentFile(attm);

            ExtMSgStr = hl7Msg.ToString();
        }

        /// <summary>
        /// There are non-ascii characters found in RDE messages, it fails hl7 to json conversion
        /// e.g. PV1|�||AD.LD^AD.278^A^&RSCH_CA_RSCH_H|
        /// </summary>
        /// <param name="seg"></param>
        /// <returns></returns>
        private string RemoveNonAsciiChar(string seg)
        {
            return Regex.Replace(seg, @"[^\u0000-\u007F]+", string.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seg"></param>
        /// <returns></returns>
        private string ValidateSegment(string seg)
        {
            //Remove newLine at the end
            var newSeg = Regex.Replace(seg, NewLinePattern, string.Empty);

            if (IsAllFieldsEmpty(newSeg))
            {
                return string.Empty;
            }

            //if all components within a field are empty, then blank out the field
            if (!seg.StartsWith("MSH|"))
            {
                newSeg = RemoveEmptyComponents(newSeg);
            }

            //|~ or ~| or ~~ causes NULL component created, then mapping exception will be raised during indexing in elasticsearch
            //therefore extra tilde needs to be removed
            newSeg = RemoveExtraTilde(newSeg);

            return newSeg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seg"></param>
        /// <returns></returns>
        private bool IsAllFieldsEmpty(string seg)
        {
            var flds = seg.Split(EnvironmentalVariable.Hl7FieldSeparator.ToCharArray());
            for (var i = 1; i < flds.Length; i++)
            {
                if (!IsFieldEmpty(flds[i]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// \* will also be treated as empty string, in RDE v2.2 message, following segment will be treated as empty segment
        /// PD1||||||||||||""
        /// </summary>
        /// <param name="fld"></param>
        /// <returns></returns>
        private bool IsFieldEmpty(string fld)
        {
            var newFld = Regex.Replace(fld, @"[\u005C|\u0022]+", string.Empty);
            if (string.IsNullOrWhiteSpace(newFld))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seg"></param>
        /// <returns></returns>
        private string RemoveEmptyComponents(string seg)
        {
            var newSeg = new StringBuilder();
            var flds = seg.Split(EnvironmentalVariable.Hl7FieldSeparator.ToCharArray());
            var filCnt = 0;
            foreach (var fld in flds)
            {
                filCnt++;
                var comps = fld.Split("~".ToCharArray());
                var compCnt = 0;
                foreach (var comp in comps)
                {
                    compCnt++;
                    //if only ^ in comp, then it is an empty component, blank it out
                    if (IsAllCompDetailsEmpty(comp))
                    {
                        newSeg.Append(string.Empty);
                    }
                    else
                    {
                        newSeg.Append(comp);
                    }  
                    if (compCnt < comps.Length)
                    {
                        newSeg.Append("~");
                    }
                }
                if (filCnt < flds.Length)
                {
                    newSeg.Append("|");
                }
            }
            return newSeg.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seg"></param>
        /// <returns></returns>
        private bool IsAllCompDetailsEmpty(string seg)
        {
            var flds = seg.Split("^".ToCharArray());
            for (var i = 0; i < flds.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(flds[i]))
                {
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="seg"></param>
        /// <returns></returns>
        private string RemoveExtraTilde(string seg)
        {
            var newSeg = seg;
            try
            {
                if (newSeg.StartsWith("GT1|") || newSeg.StartsWith("IN1|") || 
                    newSeg.StartsWith("IN2|") || newSeg.StartsWith("PID|") || 
                    newSeg.StartsWith("RXE|"))
                {
                    //handles cases like ~~~ or ~~~~, 
                    while (newSeg.Contains("~~"))
                    {
                        newSeg = newSeg.Replace("~~", "~");
                    }
                    newSeg = newSeg.Replace("|~", "|");
                    newSeg = newSeg.Replace("~|", "|");          
                }
            }
            catch { }
            return newSeg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attm"></param>
        /// <returns></returns>
        private void CreateAttachmentFile(List<string> attm)
        {
            if (MessageInfo.AttachmentType == ATTACHMENTTYPE.MDOC)
            {
                CreatePdfAttachmentFile(attm);
            }
            else if (MessageInfo.AttachmentType == ATTACHMENTTYPE.DOC || MessageInfo.AttachmentType == ATTACHMENTTYPE.RAD)
            {
                CreateTextAttachmentFile(attm);
            }
            else
            {
                Logger.LogInfo(string.Format("No attachment found in {0}", MessageInfo.Hl7Message.ToString()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attm"></param>
        private void CreatePdfAttachmentFile(List<string> attm)
        {
            if (attm.Count > 0)
            {
                var ctnt = attm.ToArray()[0];
                if (!string.IsNullOrEmpty(ctnt))
                {
                    var attmFileName = GetAttachmentFileName();
                    File.WriteAllBytes(attmFileName.FullName, Convert.FromBase64String(ctnt));
                    Logger.LogInfo(string.Format("Attachment file {0} was created.", attmFileName));
                }
            }
            else
            {
                Logger.LogInfo(string.Format("No attachment found in {0}", MessageInfo.Hl7Message.ToString()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attm"></param>
        private void CreateTextAttachmentFile(List<string> attm)
        {
            List<string> notes = new List<string>();
            foreach (var seg in attm)
            {
                var noteIdx = GetNotesFieldIndex(seg);
                if (noteIdx > -1)
                {
                    var flds = seg.Split(EnvironmentalVariable.Hl7FieldSeparator.ToCharArray());
                    if (flds.Length > noteIdx)
                    {
                        var noteFld = flds[noteIdx];
                        if (!string.IsNullOrWhiteSpace(noteFld))
                        {
                            var noteLines = noteFld.Split("~".ToCharArray());
                            foreach (var noteLine in noteLines)
                            {
                                notes.AddRange(noteLine.Split(new string[]{EnvironmentalVariable.Hl7AttachmentHtmlNewLine }, StringSplitOptions.None));
                            }
                        }
                    }
                }
            }
            if (notes.Count > 0)
            {
                var attmFileName = GetAttachmentFileName().FullName;
                File.WriteAllLines(attmFileName, notes.ToArray());
                Logger.LogInfo(string.Format("Attachment file {0} was created.", attmFileName));
            }
            else
            {
                Logger.LogInfo(string.Format("No attachment found in {0}", MessageInfo.Hl7Message.ToString()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private FileInfo GetAttachmentFileName()
        {
            var fileExt = (MessageInfo.AttachmentType == ATTACHMENTTYPE.MDOC) ? PdfAttmExt : TxtAttmExt;
            var fileName = EnvironmentalVariable.Hl7AttachmentDir + Path.DirectorySeparatorChar + 
                            MessageInfo.Type + FileNameConnector + MessageInfo.ControlNumber + FileNameConnector +
                            MessageInfo.ReceivingDate + fileExt;

            if (File.Exists(fileName))
            {
                fileName += HL7DocExtractor.GetDupFileExtension();
            }
            return new FileInfo(fileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obrSeg"></param>
        /// <returns></returns>
        private ATTACHMENTTYPE GetAttachmentType(string obrSeg)
        {
            var obrFlds = obrSeg.Split(EnvironmentalVariable.Hl7FieldSeparator.ToCharArray());
            if (obrFlds.Length > 24)
            {
                return ConvertAttachmentType(obrFlds[24]);
            }
            return ATTACHMENTTYPE.NONE;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private ATTACHMENTTYPE ConvertAttachmentType(string type)
        {
            if (ATTACHMENTTYPE.MDOC.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return ATTACHMENTTYPE.MDOC;
            }
            else if (ATTACHMENTTYPE.DOC.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return ATTACHMENTTYPE.DOC;
            }
            else if (ATTACHMENTTYPE.RAD.ToString().Equals(type, System.StringComparison.OrdinalIgnoreCase))
            {
                return ATTACHMENTTYPE.RAD;
            }
            else
            {
                return ATTACHMENTTYPE.NONE;
            }
        }
    }
}
