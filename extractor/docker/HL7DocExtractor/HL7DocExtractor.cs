using System;
using System.Threading;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    public class HL7DocExtractor
    {
        /// <summary>
        /// 
        /// </summary>
        public static ISettings Settings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static AppLogger Logger { get; set; }

        private static void ExtractDoc(string KafkaMsgVal)
        {
            if (string.IsNullOrWhiteSpace(KafkaMsgVal))
            {
                return;
            }

            Message pl = null;
            try
            {
                pl = JsonConvert.DeserializeObject<Message>(KafkaMsgVal);

                var hdlr = new HL7AttachmentExtractHandler(pl);
                hdlr.ExtractAttachment();
                if (!string.IsNullOrWhiteSpace(hdlr.ExtMSgStr))
                {
                    ProduceMessage(hdlr);
                }
            }
            catch (Exception ex)
            {
                Logger.LogInfo(string.Format("Error raised when processing {0}", (pl == null) ? string.Empty : pl.ToString()));
                Logger.LogError(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private async static void ProduceMessage(HL7AttachmentExtractHandler hdlr)
        {
            var config = new ProducerConfig { BootstrapServers = EnvironmentalVariable.Hl7MsgKafkaBootstrapServer };

            using (var p = new ProducerBuilder<Null, string>(config).Build())
            {
                try
                {
                    hdlr.MessageInfo.Hl7Message.Content = hdlr.ExtMSgStr;

                    var reqCtnt = JsonConvert.SerializeObject(hdlr.MessageInfo.Hl7Message);

                    var dr = await p.ProduceAsync(EnvironmentalVariable.Hl7DocExtractedMsgKafkaTopic, new Message<Null, string> { Value = reqCtnt });
                    Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                }
                catch (ProduceException<Null, string> e)
                {
                    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetDupFileExtension()
        {
            return EnvironmentalVariable.Hl7DuplicateFileExtension + DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Hl7 Extractor Started");
                Console.WriteLine($"Server: {EnvironmentalVariable.Hl7MsgKafkaBootstrapServer}");
                Console.WriteLine($"Producer Topic: {EnvironmentalVariable.Hl7DocExtractedMsgKafkaTopic}");
                Console.WriteLine($"Consumer Topic: {EnvironmentalVariable.Hl7RawMsgKafkaTopic}");
                Console.WriteLine($"Consumer Group Id: {EnvironmentalVariable.Hl7MsgKafkaGroupId}");

                Settings = new AppSettings();
                Logger = new AppLogger();

                var conf = new ConsumerConfig
                {
                    GroupId = EnvironmentalVariable.Hl7MsgKafkaGroupId,
                    BootstrapServers = EnvironmentalVariable.Hl7MsgKafkaBootstrapServer,
                    AutoOffsetReset = AutoOffsetReset.Earliest
                };

                using (var cb = new ConsumerBuilder<Ignore, string>(conf).Build())
                {
                    cb.Subscribe(EnvironmentalVariable.Hl7RawMsgKafkaTopic);

                    CancellationTokenSource cts = new CancellationTokenSource();
                    Console.CancelKeyPress += (_, e) =>
                    {
                        e.Cancel = true; // prevent the process from terminating.
                        cts.Cancel();
                    };

                    try
                    {
                        while (true)
                        {
                            try
                            {
                                var cr = cb.Consume(cts.Token);
                                HL7DocExtractor.ExtractDoc(cr.Value);
                            }
                            catch (ConsumeException e)
                            {
                                Console.WriteLine(string.Format("Error occured: {0}", e.Error.Reason));
                            }
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        // Ensure the consumer leaves the group cleanly and final offsets are committed.
                        cb.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }
    }
}
