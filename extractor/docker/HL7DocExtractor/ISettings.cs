namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISettings
    {
        /// <summary>
        /// 
        /// </summary>
        string ServiceName { get; }

        /// <summary>
        /// 
        /// </summary>
        string ServiceDisplayName { get; }

        /// <summary>
        /// 
        /// </summary>
        string ServiceDescription { get; }

        /// <summary>
        /// 
        /// </summary>
        //string HL7InboundDir { get; }

        /// <summary>
        /// 
        /// </summary>
        //bool HL7ArchiveOriginalMessage { get; }

        /// <summary>
        /// 
        /// </summary>
        //string HL7InboundArchiveDir { get; }

        /// <summary>
        /// 
        /// </summary>
        //string HL7InProcessingDir { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7AttachmentDir { get; }

        /// <summary>
        /// 
        /// </summary>
        //string HL7MessageDir { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7IssueDir { get; }

        /// <summary>
        /// 
        /// </summary>
        //string HL7FilebeatInboundDir { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7FieldSeparator { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7CarriageReturn { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7AttachmentHtmlNewLine { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7DupFileExt { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7FileNameSep { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7RawMsgKafkaTopic { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7DocExtractedMsgKafkaTopic { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7MsgKafkaGroupId { get; }

        /// <summary>
        /// 
        /// </summary>
        string HL7MsgKafkaBootstrapServer { get; }
    }
}
