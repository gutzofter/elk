using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    /// <summary>
    /// 
    /// </summary>
    public class Message
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string TransactionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string MessageType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string MessageControlId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string DateTimeOfMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MemberNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return MessageType + "-" + MemberNumber + "-" + MessageControlId + "-" + DateTimeOfMessage + "-" + TransactionType;
        }
    }
}
