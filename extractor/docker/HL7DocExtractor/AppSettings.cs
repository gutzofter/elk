using System.Configuration;

namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    public class AppSettings : ISettings
    { 

        /// <summary>
        /// 
        /// </summary>
        public string ServiceName
        {
            get { return ConfigurationManager.AppSettings["ServiceName"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ServiceDisplayName
        {
            get { return ConfigurationManager.AppSettings["ServiceDisplayName"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ServiceDescription
        {
            get { return ConfigurationManager.AppSettings["ServiceDescription"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        //public string HL7InboundDir
        //{
        //    get { return ConfigurationManager.AppSettings["HL7InboundDir"]; }
        //}

        /// <summary>
        /// 
        /// </summary>
        //public bool HL7ArchiveOriginalMessage
        //{
        //    get
        //    {
        //        try
        //        {
        //            return bool.Parse(ConfigurationManager.AppSettings["HL7ArchiveOriginalMessage"]);
        //        }
        //        catch
        //        {
        //            return true;
        //        }
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        //public string HL7InboundArchiveDir
        //{
        //    get { return ConfigurationManager.AppSettings["HL7InboundArchiveDir"]; }
        //}

        /// <summary>
        /// 
        /// </summary>
        //public string HL7InProcessingDir
        //{
        //    get { return ConfigurationManager.AppSettings["HL7InProcessingDir"]; }
        //}

        /// <summary>
        /// 
        /// </summary>
        public string HL7AttachmentDir
        {
            get { return ConfigurationManager.AppSettings["HL7AttachmentDir"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        //public string HL7MessageDir
        //{
        //    get { return ConfigurationManager.AppSettings["HL7MessageDir"]; }
        //}

        /// <summary>
        /// 
        /// </summary>
        public string HL7IssueDir
        {
            get { return ConfigurationManager.AppSettings["HL7IssueDir"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7FilebeatInboundDir
        {
            get { return ConfigurationManager.AppSettings["HL7FilebeatInboundDir"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7FieldSeparator
        {
            get { return ConfigurationManager.AppSettings["HL7FieldSeparator"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7CarriageReturn
        {
            get { return ConfigurationManager.AppSettings["HL7CarriageReturn"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7AttachmentHtmlNewLine
        {
            get { return ConfigurationManager.AppSettings["HL7AttachmentHtmlNewLine"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7DupFileExt
        {
            get { return ConfigurationManager.AppSettings["HL7DupFileExt"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7FileNameSep
        {
            get { return ConfigurationManager.AppSettings["HL7FileNameSep"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7RawMsgKafkaTopic
        {
            get { return ConfigurationManager.AppSettings["HL7RawMsgKafkaTopic"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7DocExtractedMsgKafkaTopic
        {
            get { return ConfigurationManager.AppSettings["HL7DocExtractedMsgKafkaTopic"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7MsgKafkaGroupId
        {
            get { return ConfigurationManager.AppSettings["HL7MsgKafkaGroupId"]; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HL7MsgKafkaBootstrapServer
        {
            get { return ConfigurationManager.AppSettings["HL7MsgKafkaBootstrapServer"]; }
        }
    }
}
