﻿using System;
using System.Configuration;

namespace Iehp.It.DataManagement.Hl7.DocExtractor
{
    public static class EnvironmentalVariable
    {
        public static ISettings Settings = new AppSettings();

        public static string TransformSetting(string environmentVariable, string settingName)
        {
            var setting = Environment.GetEnvironmentVariable(environmentVariable) ?? ConfigurationManager.AppSettings[settingName];

            return setting;
        }

        public static string Hl7AttachmentDir => TransformSetting("HL7_ATTACHMENT_FOLDER", "Hl7AttachmentDir");
        public static string Hl7FieldSeparator => TransformSetting("HL7_FIELD_SEPARATOR", "HL7FieldSeparator");
        public static string Hl7CarriageReturn => TransformSetting("HL7_CARRIAGE_RETURN", "HL7CarriageReturn");
        public static string Hl7AttachmentHtmlNewLine => TransformSetting("HL7_ATTACHMENT_HTML_NEW_LINE", "HL7AttachmentHtmlNewLine");
        public static string Hl7DuplicateFileExtension => TransformSetting("HL7_DUPLICATE_FILE_EXTENSION", "HL7DupFileExt");
        public static string Hl7FileNameSeparator => TransformSetting("HL7_FILE_NAME_SEPARATOR", "HL7FileNameSep");
        public static string Hl7RawMsgKafkaTopic => TransformSetting("HL7_CONSUMER_TOPIC", "HL7RawMsgKafkaTopic");
        public static string Hl7DocExtractedMsgKafkaTopic => TransformSetting("HL7_PRODUCER_TOPIC", "HL7DocExtractedMsgKafkaTopic");
        public static string Hl7MsgKafkaGroupId => TransformSetting("HL7_TOPIC_GROUP", "HL7MsgKafkaGroupId");
        public static string Hl7MsgKafkaBootstrapServer => TransformSetting("HL7_KAFKA_HOST", "HL7MsgKafkaBootstrapServer");
    }
}
