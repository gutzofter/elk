version: '3.7'
services:
  hl7-elastic:
    image: 'docker.elastic.co/elasticsearch/elasticsearch:${ELK_VERSION}'
    container_name: hl7-elastic
    environment:
      - node.name=hl7-elastic
      - cluster.name=es-docker-cluster
      - discovery.type=single-node
      - discovery.seed_hosts=hl7-elastic
      - bootstrap.memory_lock=true
      - ES_JAVA_OPTS=-Xms512m -Xmx512m
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - 'data01:/usr/share/elasticsearch/data'
    ports:
      - '9200:9200'
    networks:
      - elastic
  hl7-kibana:
    image: 'docker.elastic.co/kibana/kibana:${ELK_VERSION}'
    container_name: hl7-kibana
    ports:
      - '5601:5601'
    environment:
      ELASTICSEARCH_URL: 'http://hl7-elastic:9200'
      ELASTICSEARCH_HOSTS: 'http://hl7-elastic:9200'
    depends_on:
      - hl7-elastic
    networks:
      - elastic
  hl7-zookeeper:
    image: wurstmeister/zookeeper
    container_name: hl7-zookeeper
    ports:
      - '2181:2181'
    networks:
      - elastic
  hl7-kafka:
    image: wurstmeister/kafka
    container_name: hl7-kafka
    depends_on:
      - hl7-zookeeper
    ports:
      - '9092:9092'
    expose:
      - '9093'
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'hl7-zookeeper:2181'
      KAFKA_ADVERTISED_HOST_NAME: localhost
      KAFKA_ADVERTISED_LISTENERS: 'INSIDE://hl7-kafka:9093,OUTSIDE://localhost:9092'
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: 'INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT'
      KAFKA_LISTENERS: 'INSIDE://0.0.0.0:9093,OUTSIDE://0.0.0.0:9092'
      KAFKA_INTER_BROKER_LISTENER_NAME: INSIDE
    volumes:
      - '/var/run/docker.sock:/var/run/docker.sock'
    networks:
      - elastic
  hl7-extractor:
    container_name: hl7-extractor
    build: extractor/docker
    environment:
      HL7_KAFKA_HOST: 'hl7-kafka:9093'
      HL7_PRODUCER_TOPIC: 'hl7.ext'
      HL7_CONSUMER_TOPIC: 'hl7.raw'
      HL7_TOPIC_GROUP: 'hl7-extractor-consumer-group'
    volumes:
        - './data:/data'
    networks:
      - elastic
  hl7-logstash:
    build:
      context: logstash_v2/docker
      args:
        ELK_VERSION: $ELK_VERSION
    container_name: hl7-logstash
    depends_on:
      - hl7-elastic
      - hl7-kafka
    environment:
      KAFKA_HOSTNAME: 'hl7-kafka:9093'
      ELASTIC_HOSTNAME: 'http://hl7-elastic:9200'
    volumes:
      - './logstash_v2/conf:/usr/share/logstash/pipeline/'
      - './logstash_v2/log:/usr/share/logstash/logs/'
    ports:
      - '5000:5000'
      - '9600:9600'
    networks:
      - elastic      
  hl7-webapi:
    container_name: hl7-webapi 
    build: webapi/docker    
    environment:
      HL7_KAFKA_HOST: 'hl7-kafka:9093'
      HL7_PRODUCER_TOPIC: 'hl7.raw'
    depends_on:
      - hl7-kafka
    volumes:
        - './data:/data'
    ports:
      - '80:80'
      - '443:443'
    networks:
      - elastic
volumes:
  data01:
    driver: local
networks:
  elastic:
    driver: bridge
