import sys, json
from hl7apy.parser import parse_message
from kafka import KafkaConsumer, KafkaProducer

print("Message Prepare")

json_dict ={ "messagetype": "type", "message": "data"}

json_message = json.dumps(json_dict)

producer = KafkaProducer(bootstrap_servers=['kfk01:9093'])
producer.send('test', json.dumps(json_message).encode('utf-8'))
producer.flush()

print("Message Sent")
