import sys, json, os
from hl7apy.parser import parse_message
from kafka import KafkaConsumer, KafkaProducer

def get_messagetype(path):
	return path.split('/')[-1][0:3].lower()

def get_messagepath(message):
	return message["log"]["file"]["path"]

def get_message(message):
	return message["message"]

def hl7_str_to_dict(s, use_long_name=True):
    m = parse_message(s)
    return hl7_message_to_dict(m, use_long_name=use_long_name)

def hl7_message_to_dict(m, use_long_name=True):
    if m.children:
        d = {}

        for c in m.children:
            name = str(c.name).lower()

            if use_long_name:
                name = str(c.long_name).lower() if c.long_name else name

            dictified = hl7_message_to_dict(c, use_long_name=use_long_name)

            if name in d:
                if not isinstance(d[name], list):
                    d[name] = [d[name]]
                d[name].append(dictified)
            else:
                d[name] = dictified
        return d
    else:
        return m.to_er7() 

def main():	
	print("Starting HL7 to JSON Process...")
	try:
		kafkahost = os.getenv('KAFKA_HOSTNAME')
		if kafkahost is None:
			raise NameError('No KAFKA_HOSTNAME defined in $ENV')
			
		while True:
			consumer = KafkaConsumer('hl7', auto_offset_reset='earliest', bootstrap_servers=[kafkahost], group_id='hl7-group', consumer_timeout_ms=1000)
			producer = KafkaProducer(bootstrap_servers=[kafkahost])

			for message in consumer:
				message = json.loads(message.value.decode('utf8'))
				path = get_messagepath(message)
				messagetype = get_messagetype(path)
				message = get_message(message)
				
				message = message.replace('\\r', '\r')
				dict = hl7_str_to_dict(message)
				js = json.dumps(dict, sort_keys=True, indent=None)
				
				json_dict ={ "hl7messagetype": messagetype, "hl7payload": js}
				
				json_message = json.dumps(json_dict)
				
				print(json_message)
				
				producer.send('hl7.json', json_message.encode('utf-8'))
			producer.flush()
			consumer.close()

	except:
		print("Unexpected error:", sys.exc_info()[0])
		raise

if __name__ == '__main__':
	main()
