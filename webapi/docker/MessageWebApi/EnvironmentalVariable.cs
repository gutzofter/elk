using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Iehp.It.DataManagement.MessageWebApi
{
    public static class EnvironmentalVariable
    {
        public static string GetBootstrapServers(IConfiguration configuration)
        {
            var server = Environment.GetEnvironmentVariable("HL7_KAFKA_HOST") ?? configuration.GetValue<string>("AppSetting:HL7RawKafkaBootstrapServer");

            return server;
        }
        public static string GetKafkaProducerTopic(IConfiguration configuration)
        {
            var topic = Environment.GetEnvironmentVariable("HL7_PRODUCER_TOPIC") ?? configuration.GetValue<string>("AppSetting:HL7RawMsgKafkaTopic");

            return topic;
        }
    }
}
