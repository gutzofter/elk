﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Iehp.It.DataManagement.MessageWebApi.Models;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Iehp.It.DataManagement.MessageWebApi.Controllers
{
    [ApiController]
    [Route("webapiservices/documents/[controller]")]
    public class HL7Controller : ControllerBase
    {
        private IConfiguration configuration;

        public HL7Controller(IConfiguration config)
        {
            configuration = config;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public StatusCodeResult Post([FromBody]Message value)
        {
            try
            {
                MessageHandler.CreateMessageFile(configuration, value);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
