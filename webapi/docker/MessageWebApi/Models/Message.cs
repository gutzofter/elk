using System.ComponentModel.DataAnnotations;

namespace Iehp.It.DataManagement.MessageWebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Message
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string TransactionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string MessageType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string MessageControlId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string DateTimeOfMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string  ProcessingId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MemberNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Content { get; set; }
    }
}
