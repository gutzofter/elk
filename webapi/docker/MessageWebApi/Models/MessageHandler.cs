using Confluent.Kafka;
using Newtonsoft.Json;
using System;
using Microsoft.Extensions.Configuration;

namespace Iehp.It.DataManagement.MessageWebApi.Models
{
    public class MessageHandler
    {
        public static void CreateMessageFile(IConfiguration configuration, Message message)
        {
            try
            {
                PublishToKafka(configuration, message);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private async static void PublishToKafka(IConfiguration configuration, Message msgPl)
        {
            var config = new ProducerConfig { BootstrapServers = EnvironmentalVariable.GetBootstrapServers(configuration) };

            using (var p = new ProducerBuilder<Null, string>(config).Build())
            {
                try
                {
                    var reqCtnt = JsonConvert.SerializeObject(msgPl);

                    var dr = await p.ProduceAsync(EnvironmentalVariable.GetKafkaProducerTopic(configuration), new Message<Null, string> { Value = reqCtnt });
                    Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                }
                catch (ProduceException<Null, string> e)
                {
                    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                }
            }
        }
    }
}
